<?php

use Illuminate\Support\Facades\Route;

Route::any('ping', [\App\Http\Controllers\ServiceController::class, 'ping']);

// Dirty route to get the only merchant in db
Route::get('get_token', [\App\Http\Controllers\TestController::class, 'getMerchantAuthToken'])->middleware('development.only');

Route::middleware('auth:api')->group(function () {
    Route::any('run', [\App\Http\Controllers\TestController::class, 'runCode'])->middleware('development.only');
    Route::get('configuration', [\App\Http\Controllers\GetConfigurationController::class, '__invoke']);

    Route::prefix('checkout')->group(function () {
        Route::post('create', [\App\Http\Controllers\CheckoutController::class, 'create']);
        Route::post('authorise', [\App\Http\Controllers\CheckoutController::class, 'capture']);
        Route::get('{token}', [\App\Http\Controllers\CheckoutController::class, 'get']);
    });
});
