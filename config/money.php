<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Laravel money
     |--------------------------------------------------------------------------
     */
    'locale'          => config('app.locale', 'en_US'),
    'defaultCurrency' => config(CONFIG_SETTINGS_CURRENCY),
    'currencies'      => [
        'iso'         => [
            config(CONFIG_SETTINGS_CURRENCY),
        ],
    ],
];
