<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 31/07/2020
 * Time: 11:18 AM.
 */

return [
    'merchant' => [
      'username_length' => (int) env('USERNAME_LENGTH', 20),
      'password_length' => (int) env('PASSWORD_LENGTH', 64),
      'reference_number_length' => (int) env('REFERENCE_NUMBER_LENGTH', 8),
      'id_length'       => (int) env('MERCHANT_ID_LENGTH', 12),
    ],
];
