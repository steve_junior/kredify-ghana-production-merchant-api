<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 14/11/2020
 * Time: 4:11 PM.
 */

return [
    /*
     |--------------------------------------------------------------------------
     | System Settings
     |
     | The values here binds the application to the country is it set up for.
     |--------------------------------------------------------------------------
     */

    'currency'          => env('CURRENCY', 'USD'),     //ISO 4217
    'country'           => env('COUNTRY', 'GHANA'),
    'countryCodeAlpha3' => env('COUNTRY_CODE_ALPHA_3', 'GHA'), //ISO 3166-1 alpha-3
    'countryCodeAlpha2' => env('COUNTRY_CODE_ALPHA_2', 'GH'),  //ISO 3166-1 alpha-2
    'checkoutUrl'       => rtrim(env('PAYMENT_CHECKOUT_URL'), '/').'/',

    /*
     |--------------------------------------------------------------------------
     | Some settings for merchant
     |--------------------------------------------------------------------------
     */
    'merchant' => [
        'usernameLength' => (int) env('USERNAME_LENGTH', 20),
        'passwordLength' => (int) env('PASSWORD_LENGTH', 64),
        'merchantIdLength' => (int) env('MERCHANT_ID_LENGTH', 8),
    ],
];
