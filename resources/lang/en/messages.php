<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 16/11/2020
 * Time: 5:03 AM.
 */

return [

    'client_payment_status' => [
        'client_approved' => 'approved',
        'client_declined' => 'declined',
        'client_dormant'  => 'pending',
    ],

    'merchant_payment_retrieval_state' => [
        'merchant_payment_approved' => 'approved',
        'merchant_payment_declined' => 'declined',
        'merchant_payment_dormant'  => 'pending',
        'merchant_payment_complete'  => 'complete',
    ],

    'http_response' => [
        'not_found'    => 'The :attribute was not found',
        'server_error' => 'Sorry, we ran into an error on our system',
        'invalid_api_token' => 'Invalid Api Token',
        'invalid_checkout_token' => 'Invalid Checkout Token',
        'expired_checkout_token' => 'Expired Checkout Token',
    ],

    'validation' => [
        'required_parent' => 'The :attribute field is required',
        'required' => 'The :attribute field in :parent object is required',
        'url'      => 'The :attribute format is invalid',
        'email'    => 'The :attribute field in :parent must be a valid email address',
        'phone_number' => 'The :attribute field in :parent must be a valid phone number for :country',
        'amount'   => 'The :attribute field in :parent must be a valid amount',
        'currency' => 'The :attribute field in :parent must be a valid ISO 4217 currency code for :country (:symbol)',
        'integer'  => 'The :attribute field in :parent must be an integer',
        'array_parent' => 'The :attribute field must be an array of objects',
        'merchant_amount_limit' => 'The :attribute must be within your allowable amount limits',
    ],
];
