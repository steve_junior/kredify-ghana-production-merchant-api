<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 05/01/2021
 * Time: 4:58 PM
 */

namespace Tests\Feature;


use Tests\TestCase;

class WebUITest extends TestCase
{
    public function testHome()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $response->assertSeeText(config(CONFIG_APP_NAME));
    }
}
