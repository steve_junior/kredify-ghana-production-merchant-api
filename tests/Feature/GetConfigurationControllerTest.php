<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\WithFaker;
use Tests\Commoners;
use Tests\TestCase;

class GetConfigurationControllerTest extends Commoners
{
    /**
     *
     * @return void
     */
    public function testGetMerchantConfig()
    {
        $response = $this->get('/configuration', $this->getAuthorizationHeaders());

        $response->assertOk();

        $response->assertJsonStructure([
            'minimumOrderAmount' => ['value', 'currency'], 'maximumOrderAmount' => ['value', 'currency']
        ]);
    }
}
