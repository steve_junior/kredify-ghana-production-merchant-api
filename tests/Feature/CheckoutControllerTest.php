<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Commoners;
use Tests\TestCase;

class CheckoutControllerTest extends Commoners
{
    protected static $PREFIX  = "checkout/";
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->postJson(self::$PREFIX. 'create', $this->checkoutPayload(), $this->getAuthorizationHeaders());

        $response->assertCreated();

        $response->assertJsonStructure([
            'checkoutLink', 'expiresAfter', 'orderNumber', 'token'
        ]);
    }

    private function checkoutPayload()
    {
        return [
            "consumer" => [
                "phoneNumber" => "0201234566",
                "name" => "Sample User",
                "email" => "user2@gmail.com",
            ],
            "totalAmount" => [
                "value" => "353",
                "currency" => "GHS",
            ],
            "orderItems" => [
                [
                    "name"     => "Water bottle",
                    "quantity" => 6,
                    "uniqueId" => "#1898923",
                    "price" => [
                        "value"   => "78.00",
                        "currency" => "GHS"
                    ],
                    "itemUrl" => "https://www.jumia.com.gh/generic-stainless-steel-snowflakes-vacuum-flask-blue-450-ml-14887832.html"
                ]
            ],
            "deliveryCost" => [
                "value" => "5.00",
                "currency"=> "GHS"
            ],
            "orderRecipient"=> [
                "name" => "Sample User",
                "address" => "Sample user address, Accra Ghana",
                "phoneNumber"=> "0201234566"
            ],
            "discounts" => [
                [
                    "name" => "Black Friday Promo",
                    "amount" => [
                        "value" => "20.00",
                        "currency" => "GHS"
                    ]
                ]
            ],
            "orderNumber" => "#5378402313898282",
            "cancellationUrl" => "https://google.com",
            "confirmationUrl" => "https://fb.com"
        ];
    }

    private function getCheckout()
    {
        $response = $this->postJson(self::$PREFIX. 'create', $this->checkoutPayload(), $this->getAuthorizationHeaders());

        if($response->assertCreated())
            return $response;

        return null;
    }

    public function testAuthorise()
    {
        $token = $this->getCheckout()->json("token");

        $response = $this->postJson(self::$PREFIX. 'authorise',
            ['token' => $token], $this->getAuthorizationHeaders());

        $response->assertOk();

        $response->assertJsonStructure([
            'token', 'status', 'authStatus', 'authAmount', 'createdAt', 'order'
        ]);

        $response->assertJson([
            'token' => $token
        ]);
    }


    public function testGetCheckout()
    {
        $checkout = $this->getCheckout();

        $response = $this->get(self::$PREFIX . $checkout->json("token"), $this->getAuthorizationHeaders());

        $response->assertOk();

        $response->assertJsonStructure([
            'token',
            'checkoutLink',
            'expiresAfter',
            'orderNumber',
            'order' => [
                'orderRecipient', 'orderNumber', 'confirmationUrl',
                'cancellationUrl', 'consumer', 'totalAmount', 'deliveryCost', 'orderItems'
            ]
        ]);

        $response->assertJson([
            'token' => $checkout->json("token"),
            'checkoutLink' => $checkout->json("checkoutLink"),
            'expiresAfter' => $checkout->json("expiresAfter"),
            'orderNumber' => $checkout->json("orderNumber"),
        ]);
    }
}
