<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 19/11/2020
 * Time: 5:14 AM.
 */

namespace App\Http\Requests\InBuilt;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\MoneyService;

/*
 * Validation Process that steps into the json structure &
 * validates the child keys & values
 * using some custom rules.
 */

class CreateCheckoutValidation
{
    public static $CONTENT_TYPE = 'application/json';

    protected $request;

    public $messages = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function isTypeJson()
    {
        return $this->request->isJson();
    }

    public function contentType()
    {
        return is_null($this->request->getContentType()) ? 'unidentifiable' : strtolower('application/'.$this->request->getContentType());
    }

    private function cancellationUrl()
    {
        if (! $this->request->exists(Request_cancellationUrl)) {
            $error_message = __(LOCALISATION_VALIDATION_REQUIRED_PARENT, ['attribute' => Request_cancellationUrl]);
            array_push($this->messages, $error_message);
        }

        if (! isURLValid($this->request->json(Request_cancellationUrl))) {
            $error_message = __(LOCALISATION_VALIDATION_URL, ['attribute' => Request_cancellationUrl]);
            array_push($this->messages, $error_message);
        }
    }

    private function confirmationUrl()
    {
        if (! $this->request->exists(Request_confirmationUrl)) {
            $error_message = __(LOCALISATION_VALIDATION_REQUIRED_PARENT, ['attribute' => Request_confirmationUrl]);
            array_push($this->messages, $error_message);
        }

        if (! isURLValid($this->request->json(Request_confirmationUrl))) {
            $error_message = __(LOCALISATION_VALIDATION_URL, ['attribute' => Request_confirmationUrl]);
            array_push($this->messages, $error_message);
        }
    }

    private function consumer()
    {
        if (! $this->checkForRequiredParentKey(Request_consumer)) {
            return;
        }

        $this->checkForRequiredChildKeys(Request_consumer, CUSTOMER_OBJECT_KEYS);

        //check if email & phone number are valid
        $customer = $this->request->json(Request_consumer);

        $email_key = CUSTOMER_OBJECT_KEYS[1];
        if (! isEmailValid($customer[$email_key])) {
            $error_message = __(LOCALISATION_VALIDATION_EMAIL, ['attribute' => $email_key, 'parent' => Request_consumer]);
            array_push($this->messages, $error_message);
        }

        $phone_number_key = CUSTOMER_OBJECT_KEYS[2];
        if (! isPhoneNumberValid($customer[$phone_number_key])) {
            $error_message = __(LOCALISATION_VALIDATION_PHONE_NUMBER, ['attribute' => $phone_number_key, 'parent' => Request_consumer, 'country' => Str::lower(config(CONFIG_SETTINGS_COUNTRY))]);
            array_push($this->messages, $error_message);
        }
    }

    private function deliveryCost()
    {
        if (! $this->request->exists(Request_deliveryCost)) {
            return;
        }

        $this->validateAmount(Request_deliveryCost);
    }

    private function totalAmount()
    {
        if (! $this->checkForRequiredParentKey(Request_totalAmount)) {
            return;
        }

        $this->validateAmount(Request_totalAmount);

        //check if totalAmount is withing the merchant allowed limits
        $totalAmount = $this->request->json(Request_totalAmount);
        if (! isAmountWithinMerchantAllowableLimits($totalAmount)) {
            $error_message = __(LOCALISATION_VALIDATION_MERCHANT_AMOUNT_LIMIT, ['attribute' => Request_totalAmount]);
            array_push($this->messages, $error_message);
        }
    }

    private function orderRecipient()
    {
        if (! $this->checkForRequiredParentKey(Request_orderRecipient)) {
            return;
        }

        $this->checkForRequiredChildKeys(Request_orderRecipient, ORDER_RECIPIENT_OBJECT_KEYS);

        //check if phone number is valid
        $orderRecipient = $this->request->json(Request_orderRecipient);

        $phone_number_key = ORDER_RECIPIENT_OBJECT_KEYS[1];
        if (! isPhoneNumberValid($orderRecipient[$phone_number_key])) {
            $error_message = __(LOCALISATION_VALIDATION_PHONE_NUMBER, ['attribute' => $phone_number_key, 'parent' => Request_orderRecipient, 'country' => Str::lower(config(CONFIG_SETTINGS_COUNTRY))]);
            array_push($this->messages, $error_message);
        }
    }

    private function orderItems()
    {
        if ($this->request->exists(Request_orderItems)) {
            $cart_items = $this->request->json(Request_orderItems);

            if (! isArrayOfObjects($cart_items)) {
                $error_message = __(LOCALISATION_VALIDATION_ARRAY_PARENT, ['attribute' => Request_orderItems]);
                array_push($this->messages, $error_message);
            } else {
                foreach ($cart_items as $index => $cart_item) {
                    $this->checkForRequiredChildKeys(Request_orderItems.'.'.$index, ORDER_ITEMS_OBJECT_KEYS);

                    //check if URLs are valid
                    $item_url_key = ORDER_ITEMS_OBJECT_KEYS[4];
                    if (! isURLValid($cart_item[$item_url_key])) {
                        $error_message = __(LOCALISATION_VALIDATION_URL, ['attribute' => $item_url_key, 'parent' => "cartItems[{$index}]"]);
                        array_push($this->messages, $error_message);
                    }

                    //check if the quantity is positive integer
                    $quantity_key = ORDER_ITEMS_OBJECT_KEYS[1];
                    if (! isPositiveDigit($cart_item[$quantity_key])) {
                        $error_message = __(LOCALISATION_VALIDATION_INTEGER, ['attribute' => $quantity_key, 'parent' => "cartItems[{$index}]"]);
                        array_push($this->messages, $error_message);
                    }

                    //check if the amount & currency are valid
                    $price_key = Request_orderItems.'.'.$index.'.'.ORDER_ITEMS_OBJECT_KEYS[3];
                    $this->validateAmount($price_key);
                }
            }
        }
    }

    private function discounts()
    {
        if ($this->request->exists(Request_discounts)) {
            $discounts = $this->request->json(Request_discounts);

            if (! isArrayOfObjects($discounts)) {
                $error_message = __(LOCALISATION_VALIDATION_ARRAY_PARENT, ['attribute' => Request_discounts]);
                array_push($this->messages, $error_message);
            } else {
                foreach ($discounts as $index => $discount) {
                    $this->checkForRequiredChildKeys(Request_discounts.'.'.$index, DISCOUNT_OBJECT_KEYS);

                    //check if the value & currency in amount are valid
                    $amount_key = Request_discounts.'.'.$index.'.'.DISCOUNT_OBJECT_KEYS[1];
                    $this->validateAmount($amount_key);
                }
            }
        }
    }

    private function orderNumber()
    {
        if (! $this->request->exists(Request_orderNumber)) {
            $error_message = __(LOCALISATION_VALIDATION_REQUIRED_PARENT, ['attribute' => Request_orderNumber]);
            array_push($this->messages, $error_message);
        }
    }

    public function validate()
    {
        //required fields
        $this->cancellationUrl();

        $this->confirmationUrl();

        $this->consumer();

        $this->totalAmount();

        $this->orderRecipient();

        $this->orderNumber();

        //optional fields
        $this->deliveryCost();

        $this->orderItems();

        $this->discounts();

        return $this;
    }

    /*
     * Check for valid json keys in any selected parent key
     */
    private function checkForRequiredChildKeys($parent_key, $child_keys)
    {
        $payload = $this->request->json($parent_key);

        //Check if keys exists
        $matched_keys = arrayKeysIsset($child_keys, $payload);

        if (! $matched_keys->all_set) {
            $error_message = __(LOCALISATION_VALIDATION_REQUIRED, ['attribute' => $matched_keys->missing_key, 'parent' => $parent_key]);
            array_push($this->messages, $error_message);
        }
    }

    private function checkForRequiredParentKey($key)
    {
        //Check if required key exists
        $is_present = $this->request->exists($key);

        if (! $is_present) {
            $error_message = __(LOCALISATION_VALIDATION_REQUIRED_PARENT, ['attribute' => $key]);
            array_push($this->messages, $error_message);
        }

        return $is_present;
    }

    private function validateAmount($key)
    {
        $this->checkForRequiredChildKeys($key, MONEY_OBJECT_KEYS);

        //check if amount value & currency comply with the system value
        $amount = $this->request->json($key);

        $value_key = MONEY_OBJECT_KEYS[0];
        if (array_key_exists($value_key, $amount) && ! MoneyService::isAmountValid($amount[$value_key])) {
            $error_message = __(LOCALISATION_VALIDATION_AMOUNT, [
                'attribute' => $value_key,
                'parent'    => $key,
            ]);
            array_push($this->messages, $error_message);
        }

        $currency_key = MONEY_OBJECT_KEYS[1];
        if (array_key_exists($currency_key, $amount) && ! MoneyService::isCurrencyValid($amount[$currency_key])) {
            $error_message = __(LOCALISATION_VALIDATION_CURRENCY, [
                'attribute' => $currency_key,
                'parent'    => $key,
                'country'   => ucfirst(strtolower(config(CONFIG_SETTINGS_COUNTRY))),
                'symbol'    => config(CONFIG_SETTINGS_CURRENCY),
            ]);
            array_push($this->messages, $error_message);
        }
    }
}
