<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 20/07/2020
 * Time: 4:52 AM.
 */

use App\Services\LogService;

if (! function_exists('isURLValid')) {
    function isURLValid($url)
    {
        $url = strtolower($url);

        return in_array(substr($url, 0, 5), ['https', 'http:']) && filter_var($url, FILTER_VALIDATE_URL);
    }
}

if (! function_exists('isEmailValid')) {
    function isEmailValid($email)
    {
        $email_sanitised = filter_var($email, FILTER_SANITIZE_EMAIL);

        return filter_var($email_sanitised, FILTER_VALIDATE_EMAIL);
    }
}

if (! function_exists('isPhoneNumberValid')) {
    function isPhoneNumberValid($phone_number)
    {
        $is_valid = false;

        try {
            $phone = phone($phone_number, [config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2)]);
            $is_valid = $phone->isOfCountry(config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2));
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_ParseValidationError, $exception);
        }

        return $is_valid;
    }
}

if (! function_exists('isPositiveDigit')) {
    function isPositiveDigit($value)
    {
        return is_int($value) && (int) $value >= 0;
    }
}

if (! function_exists('isArrayOfObjects')) {
    function isArrayOfObjects($array)
    {
        return is_array($array) && isset($array[0]);
    }
}

if (! function_exists('isAmountWithinMerchantAllowableLimits')) {
    function isAmountWithinMerchantAllowableLimits($amount)
    {
        if (is_array($amount)) {
            if (! isset($amount['value'])) {
                return false;
            } else {
                $order_value = floatval($amount['value']);
            }
        } elseif (is_object($amount)) {
            if (! isset($amount->value)) {
                return false;
            } else {
                $order_value = floatval($amount->value);
            }
        } else {
            return false;
        }

        $merchant = auth()->user();
        $min_value = floatval($merchant->minimum_order_amount->value);
        $max_value = floatval($merchant->maximum_order_amount->value);

        $higher_than_min_value = $order_value - $min_value >= 0;
        $lesser_than_max_value = $max_value - $order_value >= 0;

        return $higher_than_min_value && $lesser_than_max_value;
    }
}
