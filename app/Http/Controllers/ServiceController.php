<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 13/11/2020
 * Time: 9:06 PM.
 */

namespace App\Http\Controllers;

use App\Services\LogService;
use App\Services\ResponseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Transformers\ResponseStructure;

class ServiceController extends Controller
{
    /*
     * Responds 'pong' if items in checklist passes
     * Checklist:
     *      1. Is the database online
     *      2. Is the app out of maintenance mode
     */
    public function ping()
    {
        $database_online = false;

        try {
            if (DB::connection()->getPdo()) {
                $database_online = true;
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_DatabaseConnectionError, $exception);
        }

        $message = $database_online && ! App::isDownForMaintenance() ? 'pong' : '';

        return ResponseStructure::success($message);
    }
}
