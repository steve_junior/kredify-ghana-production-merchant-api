<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 20/07/2020
 * Time: 4:26 AM.
 */

namespace App\Http\Controllers;

use App\Models\Checkout;
use App\Services\LogService;

use Illuminate\Http\Request;
use App\Services\CheckoutService;
use App\Services\ResponseService;
use Illuminate\Support\Facades\DB;
use App\Models\Merchant\PaymentEvent;
use App\Repositories\OrderRepository;
use App\Transformers\ResponseStructure;
use App\Repositories\CheckoutRepository;
use App\Repositories\MerchantEventRepository;
use App\Http\Requests\AuthorisePaymentRequest;
use App\Http\Requests\InBuilt\CreateCheckoutValidation;

class CheckoutController extends Controller
{
    protected $checkouts;

    protected $orders;

    protected $checkoutService;

    public function __construct(CheckoutRepository $checkoutRepository, OrderRepository $orderRepository, CheckoutService $checkoutService)
    {
        $this->checkouts = $checkoutRepository;
        $this->orders = $orderRepository;
        $this->checkoutService = $checkoutService;
    }

    /*
     * Initialises a checkout token for the customer
     * attempting to make a purchase.
     *
     * Function needs more improvement in handling errors that
     * could occurs from missing keys in the json body.
     *
     * A way to check if the different keys are available in
     * the json body is important
     */
    public function create(Request $request)
    {
        $validator = new CreateCheckoutValidation($request);

        if (! $validator->isTypeJson()) {
            $message = 'Expected '.$validator::$CONTENT_TYPE.' content-type';
            $message .= ' but got '.$validator->contentType();

            return ResponseStructure::error($message, BAD_REQUEST_HTTP_CODE);
        } else {
            $validator->validate();

            if (count($validator->messages) > 0) {
                return ResponseStructure::error('validation error', UNPROCESSABLE_ENTITY_HTTP_CODE, $validator->messages);
            }
        }

        $data = null;

        try {
            DB::beginTransaction();

            $order = $this->orders->store($request);

            // There is no need for merchant order number to be unique,
            // since many merchant can create same order number coincidentally
            // Once order is created, generate a unique link for the checkout URL

            $checkout = $this->checkouts->create($order);
            $data = ResponseService::createCheckout($checkout);
        } catch (\Exception $exception) {
            DB::rollBack();
            LogService::ErrorLog(Log_CreateCheckoutError, $exception);

            return ResponseStructure::error(__(LOCALISATION_RESPONSES_SERVERERROR), SERVER_ERROR_HTTP_CODE, $exception->getMessage());
        }

        DB::commit();

        return  ResponseStructure::success($data, RESOURCE_CREATED_HTTP_CODE);
    }

    /*
     * Get an incomplete individual checkout by token.
     */
    public function get($token)
    {
        $checkout = $this->checkouts->findToken($token);

        if (is_null($checkout)) {
            return ResponseStructure::error('token not found', NOT_FOUND_HTTP_CODE);
        }

        //check if the token is still active.
        if ($checkout->hasExpired()) {
            return ResponseStructure::error('expired token', OK_HTTP_CODE);
        }

        $data = ResponseService::createCheckout($checkout);
        $data->order = ResponseService::createOrder($checkout);

        return ResponseStructure::success($data);
    }

    /**
     * With the checkout token, the merchant can initiate the process
     * of reconciling the FULL AMOUNT of the  consumer's order to their
     * payout account.
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function capture(Request $request)
    {
        if (! $request->exists(Request_token)) {
            return ResponseStructure::error(__('validation.required', ['attribute' => Request_token]), UNPROCESSABLE_ENTITY_HTTP_CODE);
        }

        $checkout = $this->checkouts->findToken($request->get(Request_token));

        if (! ($checkout instanceof Checkout)) {
            return ResponseStructure::error(__(LOCALISATION_RESPONSES_INVALID_CHECKOUT_TOKEN_ERROR), BAD_REQUEST_HTTP_CODE);
        }

        if ($checkout->hasExpired()) {
            return ResponseStructure::error(__(LOCALISATION_RESPONSES_EXPIRED_CHECKOUT_TOKEN_ERROR), BAD_REQUEST_HTTP_CODE);
        }

        $data = null;

        try {
            DB::beginTransaction();

            $paymentEvent = $this->checkoutService->processAuthorisation($checkout);

            $data = ResponseService::createPayment($checkout, $paymentEvent);
        } catch (\Exception $exception) {
            DB::rollBack();
            LogService::ErrorLog(Log_CreateMerchantPaymentError, $exception);

            return ResponseStructure::error(__(LOCALISATION_RESPONSES_SERVERERROR), SERVER_ERROR_HTTP_CODE);
        }
        DB::commit();

        return ResponseStructure::success($data);
    }
}
