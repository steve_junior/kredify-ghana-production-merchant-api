<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 19/07/2020
 * Time: 5:54 PM.
 */

namespace App\Http\Controllers;

use App\Services\ResponseService;
use App\Transformers\ResponseStructure;
use App\Models\Merchant\Account as MerchantAccount;

class GetConfigurationController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __invoke()
    {
        $authMerchant = auth()->user();

        if ($authMerchant instanceof MerchantAccount) {
            return ResponseStructure::success(ResponseService::createMerchantConfiguration($authMerchant));
        }

        return  ResponseStructure::error(__(LOCALISATION_RESPONSES_INVALID_API_TOKEN_ERROR), UNAUTHENTICATED_HTTP_CODE);
    }
}
