<?php

namespace App\Http\Controllers;

use App\Services\MoneyService;
use App\Models\Merchant\Account;
use App\Transformers\ResponseStructure;

class TestController extends Controller
{
    protected $moneyService;

    public function __construct(MoneyService $moneyService)
    {
        $this->moneyService = $moneyService;
    }

    public function runCode()
    {
        $data = init();

        $data->response2 = isPhoneNumberValid('+552515858');

        return ResponseStructure::success($data);
    }

    /*
     * Must remove in production
     */
    public function getMerchantAuthToken()
    {
        return Account::all()->first()->production_api_token;
    }
}
