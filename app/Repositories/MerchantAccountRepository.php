<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 02/08/2020
 * Time: 6:19 AM.
 */

namespace App\Repositories;

use App\Models\Merchant\Account as MerchantAccount;

class MerchantAccountRepository
{
    public function find($key, $value)
    {
        return MerchantAccount::all()->firstWhere($key, $value);
    }
}
