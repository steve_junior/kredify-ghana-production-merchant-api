<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/08/2020
 * Time: 1:40 PM.
 */

namespace App\Repositories;

use App\Models\Order;
use App\Models\Checkout;
use App\Services\GeneratorService;

class CheckoutRepository
{
    public function find($key, $value)
    {
        return Checkout::all()->firstWhere($key, $value);
    }

    public function findToken($token)
    {
        return $this->find('token', $token);
    }

    public function random()
    {
        return Checkout::all()->random()->first();
    }

    public function create(Order $order)
    {
        return Checkout::create(['token' => GeneratorService::uuidString(), 'order_id' => $order->id]);
    }
}
