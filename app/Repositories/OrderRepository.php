<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/08/2020
 * Time: 6:07 PM.
 */

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderRepository
{
    const MERCHANT_ORDER_NUMBER_COLUMN = 'merchant_order_number';

    public function find($key, $value)
    {
        return Order::all()->firstWhere($key, $value);
    }

    public function findMerchantOrderNumber($token)
    {
        return $this->find(self::MERCHANT_ORDER_NUMBER_COLUMN, $token);
    }

    public function findAndUpdate($key, $value, array $attributes = [])
    {
        $model = $this->find($key, $value);

        if ($model instanceof Order) {
            return $model->updateSimpleModel($attributes);
        }

        return false;
    }

    public function findAndUpdateOrderNumber($old = null, $new = null)
    {
        if (isset($old) && isset($new)) {
            return  $this->findAndUpdate(self::MERCHANT_ORDER_NUMBER_COLUMN, $old, [self::MERCHANT_ORDER_NUMBER_COLUMN => $new]);
        }

        return false;
    }

    public function store(Request $request)
    {
        return Order::create([
            'consumer'         => $request->get(Request_consumer),
            'order_items'      => $request->json(Request_orderItems),
            'order_recipient'  => $request->json(Request_orderRecipient),
            'delivery_cost'    => $request->json(Request_deliveryCost),
            'discounts'        => $request->json(Request_discounts),
            'total_amount'     => $request->json(Request_totalAmount),
            'confirmation_url' => $request->json(Request_confirmationUrl),
            'cancellation_url' => $request->json(Request_cancellationUrl),
            'order_number'     => $request->get(Request_orderNumber),
        ]);
    }
}
