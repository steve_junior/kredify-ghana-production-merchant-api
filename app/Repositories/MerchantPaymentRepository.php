<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 02/08/2020
 * Time: 3:41 PM.
 */

namespace App\Repositories;

use Exception;
use App\Services\LogService;
use App\Models\Merchant\Payment as MerchantPayment;

class MerchantPaymentRepository
{
    public function find($key, $value)
    {
        return MerchantPayment::all()->where($key, $value);
    }

    public function findByAttributes(array $attributes = [])
    {
        if (empty($attributes)) {
            return;
        }

        $payments = MerchantPayment::all();

        foreach ($attributes as $key => $attribute) {
            $payments = $payments->where($key, $attribute);
        }

        if ($payments->count() > 1) {
            LogService::ErrorLog(Log_MultipleUniquePaymentRecordError, new Exception('Query for merchant payment by attribute cannot return more than 1 object', 500));
        }

        return $payments->first();
    }
}
