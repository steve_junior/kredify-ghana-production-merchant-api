<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 19/07/2020
 * Time: 3:44 AM.
 */
const CUSTOMER_OBJECT_KEYS = ['name', 'email', 'phoneNumber'];
const ORDER_RECIPIENT_OBJECT_KEYS = ['name', 'phoneNumber', 'address'];
const MONEY_OBJECT_KEYS = ['value', 'currency'];
const ORDER_ITEMS_OBJECT_KEYS = ['name', 'quantity', 'uniqueId', 'price', 'itemUrl'];
const DISCOUNT_OBJECT_KEYS = ['name', 'amount'];

//http Code
const SERVER_ERROR_HTTP_CODE = 500;

const BAD_REQUEST_HTTP_CODE = 400;
const UNPROCESSABLE_ENTITY_HTTP_CODE = 422;
const NOT_FOUND_HTTP_CODE = 404;
const GONE_HTTP_CODE = 410;
const UNAUTHENTICATED_HTTP_CODE = 401;

const RESOURCE_CREATED_HTTP_CODE = 201;
const OK_HTTP_CODE = 200;

//Client Payment Statuses
const CLIENT_APPROVED = 800;
const CLIENT_DECLINED = 801;
const CLIENT_DORMANT = 802;

//Payment Retrieval State
const MERCHANT_PAYMENT_APPROVED = 700;
const MERCHANT_PAYMENT_DECLINED = 701;
const MERCHANT_PAYMENT_DORMANT = 702;
const MERCHANT_PAYMENT_COMPLETE = 703;

const PAYMENT_SUCCESS = 'success';
const PAYMENT_FAILED = 'failed';
const PAYMENT_PENDING = 'pending';
const PAYMENT_INITIATED = 'initiated';
const PAYMENT_STATUSES = [PAYMENT_SUCCESS, PAYMENT_FAILED, PAYMENT_PENDING, PAYMENT_INITIATED];
