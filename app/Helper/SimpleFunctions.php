<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 15/11/2020
 * Time: 4:18 PM.
 */
if (! function_exists('init')) {
    function init()
    {
        return new StdClass;
    }
}

if (! function_exists('isNotNull')) {
    function isNotNull($value)
    {
        return ! is_null($value);
    }
}

if (! function_exists('hundredth')) {
    function hundredth($value)
    {
        return floatval($value) / 100;
    }
}

if (! function_exists('isAppInProduction')) {
    function isAppInProduction()
    {
        return env('APP_ENV') === 'production';
    }
}

if (! function_exists('getInternationalPhone')) {
    function getInternationalPhone($phone_number)
    {
        return phone($phone_number, config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2))->formatInternational();
    }
}
