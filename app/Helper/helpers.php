<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 18/07/2020
 * Time: 9:56 PM.
 */

//Customer Checkout Status
if (! function_exists('customerCheckoutStatus')) {
    function customerCheckoutStatus($key = null)
    {
        $data = [
            CLIENT_APPROVED => __(LOCALISATION_CLIENT_PAYMENT_APPROVED),
            CLIENT_DECLINED => __(LOCALISATION_CLIENT_PAYMENT_DECLINED),
            CLIENT_DORMANT  => __(LOCALISATION_CLIENT_PAYMENT_DORMANT),
        ];

        if ($key == null) {
            return $data;
        } else {
            return $data[$key];
        }
    }
}

//Merchant Retrieval Status
if (! function_exists('merchantAuthorisationStatus')) {
    function merchantAuthorisationStatus($key = null)
    {
        $data = [
            MERCHANT_PAYMENT_APPROVED  => __(LOCALISATION_MERCHANT_PAYMENT_APPROVED),
            MERCHANT_PAYMENT_DECLINED  => __(LOCALISATION_MERCHANT_PAYMENT_DECLINED),
            MERCHANT_PAYMENT_DORMANT   => __(LOCALISATION_MERCHANT_PAYMENT_DORMANT),
            MERCHANT_PAYMENT_COMPLETE  => __(LOCALISATION_MERCHANT_PAYMENT_COMPLETE),
        ];

        if ($key == null) {
            return $data;
        } else {
            return $data[$key];
        }
    }
}
