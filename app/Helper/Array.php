<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 15/11/2020
 * Time: 4:18 PM.
 */

/*
 * Array helpers
 */

if (! function_exists('arrayKeysIsset')) {
    /**
     * @param array $keys
     * @param array $subject
     * @return StdClass
     */
    function arrayKeysIsset(array $keys, array $subject)
    {
        $result = init();
        $result->all_set = true;
        foreach ($keys as $index => $key) {
            if (! isset($subject[$key])) {
                $result->all_set = false;
                $result->missing_key = $key;

                break;
            }
        }

        return $result;
    }
}
