<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 13/11/2020
 * Time: 10:28 PM.
 */
const CONFIG_SETTINGS = 'app_settings.';
const CONFIG_SETTINGS_CURRENCY = CONFIG_SETTINGS.'currency';
const CONFIG_SETTINGS_COUNTRY = CONFIG_SETTINGS.'country';
const CONFIG_SETTINGS_COUNTRYCODE_ALPHA3 = CONFIG_SETTINGS.'countryCodeAlpha3';
const CONFIG_SETTINGS_COUNTRYCODE_ALPHA2 = CONFIG_SETTINGS.'countryCodeAlpha2';
const CONFIG_SETTINGS_CHECKOUTURL = CONFIG_SETTINGS.'checkoutUrl';

const CONFIG_SETTINGS_MERCHANT = CONFIG_SETTINGS.'merchant.';
const CONFIG_SETTINGS_MERCHANT_USERNAMELENGTH = CONFIG_SETTINGS_MERCHANT.'usernameLength';
const CONFIG_SETTINGS_MERCHANT_PASSWORDLENGTH = CONFIG_SETTINGS_MERCHANT.'passwordLength';
const CONFIG_SETTINGS_MERCHANT_ID_LENGTH = CONFIG_SETTINGS_MERCHANT.'merchantIdLength';

const CONFIG_APP_TYPE = 'app.type';
const CONFIG_APP_NAME = 'app.name';

//Localisation Strings
const LOCALISATION_RESPONSES_NOTFOUND = 'messages.http_response.not_found';
const LOCALISATION_RESPONSES_SERVERERROR = 'messages.http_response.server_error';
const LOCALISATION_RESPONSES_INVALID_API_TOKEN_ERROR = 'messages.http_response.invalid_api_token';
const LOCALISATION_RESPONSES_INVALID_CHECKOUT_TOKEN_ERROR = 'messages.http_response.invalid_checkout_token';
const LOCALISATION_RESPONSES_EXPIRED_CHECKOUT_TOKEN_ERROR = 'messages.http_response.expired_checkout_token';

const LOCALISATION_CLIENT_PAYMENT_APPROVED = 'messages.client_payment_status.client_approved';
const LOCALISATION_CLIENT_PAYMENT_DECLINED = 'messages.client_payment_status.client_declined';
const LOCALISATION_CLIENT_PAYMENT_DORMANT = 'messages.client_payment_status.client_dormant';

const LOCALISATION_MERCHANT_PAYMENT_DORMANT = 'messages.merchant_payment_retrieval_state.merchant_payment_dormant';
const LOCALISATION_MERCHANT_PAYMENT_DECLINED = 'messages.merchant_payment_retrieval_state.merchant_payment_declined';
const LOCALISATION_MERCHANT_PAYMENT_APPROVED = 'messages.merchant_payment_retrieval_state.merchant_payment_approved';
const LOCALISATION_MERCHANT_PAYMENT_COMPLETE = 'messages.merchant_payment_retrieval_state.merchant_payment_complete';

const LOCALISATION_VALIDATION_REQUIRED_PARENT = 'messages.validation.required_parent';
const LOCALISATION_VALIDATION_REQUIRED = 'messages.validation.required';
const LOCALISATION_VALIDATION_URL = 'messages.validation.url';
const LOCALISATION_VALIDATION_EMAIL = 'messages.validation.email';
const LOCALISATION_VALIDATION_PHONE_NUMBER = 'messages.validation.phone_number';
const LOCALISATION_VALIDATION_AMOUNT = 'messages.validation.amount';
const LOCALISATION_VALIDATION_CURRENCY = 'messages.validation.currency';
const LOCALISATION_VALIDATION_INTEGER = 'messages.validation.integer';
const LOCALISATION_VALIDATION_ARRAY_PARENT = 'messages.validation.array_parent';
const LOCALISATION_VALIDATION_MERCHANT_AMOUNT_LIMIT = 'messages.validation.merchant_amount_limit';

//Error Log Types
const Log_MoneyParseError = 'MoneyParseError';
const Log_CreateCheckoutError = 'CreateCheckoutError';
const Log_CreateMerchantPaymentError = 'CreateMerchantPaymentError';
const Log_MultipleUniquePaymentRecordError = 'MultipleUniquePaymentRecordError';
const Log_DatabaseConnectionError = 'DatabaseConnectionError';
const Log_ParseValidationError = 'ParseValidationError';

//Request Parameters
const Request_consumer = 'consumer';
const Request_totalAmount = 'totalAmount';
const Request_orderItems = 'orderItems';
const Request_deliveryCost = 'deliveryCost';
const Request_orderRecipient = 'orderRecipient';
const Request_discounts = 'discounts';
const Request_orderNumber = 'orderNumber';
const Request_cancellationUrl = 'cancellationUrl';
const Request_confirmationUrl = 'confirmationUrl';
const Request_token = 'token';
