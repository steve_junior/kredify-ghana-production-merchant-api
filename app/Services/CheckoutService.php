<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 06/12/2020
 * Time: 3:28 PM.
 */

namespace App\Services;

use App\Models\Checkout;
use App\Models\Merchant\PaymentEvent;
use App\Repositories\MerchantEventRepository;

class CheckoutService
{
    protected $merchantEvents;

    public function __construct(MerchantEventRepository $merchantEventRepository)
    {
        $this->merchantEvents = $merchantEventRepository;
    }

    /**
     * @param Checkout $checkout
     * @return \StdClass
     * @throws \Exception
     */
    public function processAuthorisation(Checkout $checkout)
    {
        $event = init();

        $event->amount = $checkout->order->total_amount;

        if ($checkout->isDeclinedByConsumer()) {
            $event->auth_status = merchantAuthorisationStatus(MERCHANT_PAYMENT_DECLINED);
        } elseif ($checkout->isPendingByConsumer()) {
            $event->auth_status = merchantAuthorisationStatus(MERCHANT_PAYMENT_DORMANT);
        } elseif ($checkout->isApprovedByConsumer()) {
            $event->auth_status = merchantAuthorisationStatus(MERCHANT_PAYMENT_APPROVED);

            $paymentEvent = $this->merchantEvents->findOne(['checkout_id' => $checkout->id, 'merchant_account_id' => auth()->id()], [], PaymentEvent::class);

            if (is_null($paymentEvent)) {
                $paymentEvent = PaymentEvent::create([
                    'status'        => MERCHANT_PAYMENT_APPROVED,
                    'total_amount'  => $checkout->order->total_amount,
                    'checkout_id'   => $checkout->id,
                ]);
            }

            $event->event_id = $paymentEvent->event_id;
            $event->auth_at = $paymentEvent->created_at;
        } else {
            throw new \Exception('Unknown checkout status');
        }

        return $event;
    }
}
