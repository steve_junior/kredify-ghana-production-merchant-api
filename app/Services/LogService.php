<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 14/11/2020
 * Time: 10:07 AM.
 */

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Log;

class LogService
{
    protected static function format($logType, Exception $exception)
    {
        $message = $exception->getMessage().' >> ';
        $message .= $exception->getFile().' >> ';
        $message .= 'line '.$exception->getLine();

        return $logType.': '.$message;
    }

    public static function ErrorLog($logType, Exception $exception)
    {
        Log::error(self::format($logType, $exception));
    }
}
