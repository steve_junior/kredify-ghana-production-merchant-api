<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 14/11/2020
 * Time: 8:27 AM.
 */

namespace App\Services;

use Exception;
use Cknow\Money\Money;

class MoneyService
{
    /*
     * Parses the raw amount like 2.50 to standard system amount object
     * return Money | null
     */
    private static function parse($value)
    {
        $money = null;

        try {
            if (! is_numeric($value)) {
                throw new Exception('Parsed amount can only numeric');
            }

            if (floatval($value) < 0) {
                throw new Exception('Parsed amount cannot be negative');
            }

            $amount_in_string = number_format(floatval($value), 2, '.', '');
            $money = Money::parseByDecimal($amount_in_string, config(CONFIG_SETTINGS_CURRENCY));
        } catch (Exception $exception) {
            LogService::ErrorLog(Log_MoneyParseError, $exception);

            return;
        }

        return $money;
    }

    public static function isAmountValid($amount)
    {
        return isNotNull(self::parse($amount));
    }

    public static function isCurrencyValid($currency)
    {
        return $currency === config(CONFIG_SETTINGS_CURRENCY);
    }

    public static function getAmount($amount)
    {
        $money = self::parse($amount);

        $data = init();

        $data->value = hundredth($money->getAmount());
        $data->currency = $money->getCurrency()->getCode();
        $data->tag = $money->formatByIntl();

        return $data;
    }

    public static function getMoney($amount)
    {
        return self::parse($amount);
    }
}
