<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 15/11/2020
 * Time: 4:37 PM.
 */

namespace App\Services;

use App\Models\Checkout;
use App\Models\Merchant\Account as MerchantAccount;

/*
 * Responsible for orchestrating certain objects
 * that are comprehensive to the merchant
 * as part of the response object attached to API responses
 *
 */
class ResponseService
{
    public static function createCheckout(Checkout $checkout)
    {
        $data = init();

        $data->token = $checkout->token;
        $data->checkoutLink = $checkout->token_url;
        $data->expiresAfter = $checkout->expired_at;
        $data->orderNumber  = isset($checkout->order->order_number) ? $checkout->order->order_number : $checkout->order->system_generated_order_number;

        return $data;
    }

    public static function createOrder(Checkout $checkout)
    {
        $data = init();

        $checkoutOrder = optional($checkout->order);

        $data->consumer = $checkoutOrder->consumer;
        $data->totalAmount = $checkoutOrder->total_amount;
        $data->deliveryCost = $checkoutOrder->delivery_cost;
        $data->orderItems = $checkoutOrder->order_items;
        $data->orderRecipient = $checkoutOrder->order_recipient;
        $data->orderNumber = isset($checkoutOrder->order_number) ? $checkoutOrder->order_number : $checkoutOrder->system_generated_order_number;
        $data->confirmationUrl = $checkoutOrder->confirmation_url;
        $data->cancellationUrl = $checkoutOrder->cancellation_url;

        return $data;
    }

    /**
     * @param Checkout $checkout
     * @param $payment
     * @return \StdClass
     */
    public static function createPayment(Checkout $checkout, $payment)
    {
        $data = init();

        if ($payment->auth_status === merchantAuthorisationStatus(MERCHANT_PAYMENT_APPROVED)) {
            $data->authId = $payment->event_id;
            $data->authAt = $payment->auth_at;
        }

        $data->token = $checkout->token;
        $data->status = $checkout->status;
        $data->authStatus = $payment->auth_status;
        $data->createdAt = $checkout->created_at;
        $data->authAmount = $payment->amount;

        $orderObject = self::createOrder($checkout);
        $order = init();
        $order->items = $orderObject->orderItems;
        $order->amount = $orderObject->totalAmount;

        $data->order = $order;

        return $data;
    }

    /**
     * @param $amount
     * @return \StdClass
     * @throws \Exception
     */
    public static function createAmount($amount)
    {
        if (isset($amount->value) && isset($amount->currency)) {
            return $amount;
        }

        if (isset($amount->value) && ! isset($amount->currency)) {
            $amount = $amount->value;
        }

        if (is_numeric($amount)) {
            return MoneyService::getAmount($amount);
        }

        throw new \Exception('Invalid amount passed');
    }

    /**
     * @param MerchantAccount $account
     * @return \StdClass
     * @throws \Exception
     */
    public static function createMerchantConfiguration(MerchantAccount $account)
    {
        $data = init();

//        $data->merchantId = $account->merchant_id;
//        $data->merchant   = $account->name;
        $data->minimumOrderAmount = self::createAmount($account->minimum_order_amount);
        $data->maximumOrderAmount = self::createAmount($account->maximum_order_amount);

        return $data;
    }
}
