<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 15/11/2020
 * Time: 4:01 PM.
 */

namespace App\Services;

use Illuminate\Support\Str;
use App\Models\Merchant\Account as MerchantAccount;

class GeneratorService
{
    public static function generateApiToken($username = null, $password = null)
    {
        if (is_null($username) || is_null($password)) {
            return;
        }
        $token = $username.':'.$password;

        return hash('sha256', $token);
    }

    public static function generateOrderNumber()
    {
        return self::numericString();
    }

    public static function generateMerchantUsername($account_name = null)
    {
        if (is_null($account_name) || strlen($account_name) == 0) {
            return self::randomString(config(CONFIG_SETTINGS_MERCHANT_USERNAMELENGTH));
        }

        $names_array = explode(' ', $account_name);
        $short_name = $names_array[0];
        $random = self::randomString(config(CONFIG_SETTINGS_MERCHANT_USERNAMELENGTH) - strlen($short_name));

        return strtolower($short_name.$random);
    }

    public static function generateMerchantPassword()
    {
        $random = strtolower(self::randomString(config(CONFIG_SETTINGS_MERCHANT_PASSWORDLENGTH)));

        return base64_encode($random);
    }

    public static function randomString($a)
    {
        $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $c = strlen($x) - 1;
        $z = '';
        for ($i = 0; $i < $a; $i++) {
            $y = rand(0, $c);
            $z .= substr($x, $y, 1);
        }

        return $z;
    }

    public static function generateMerchantID()
    {
        do {
            $reference = (int) substr(self::numericString(), 0, config(CONFIG_SETTINGS_MERCHANT_ID_LENGTH));
            $merchant = MerchantAccount::all()->firstWhere('merchant_id', $reference);
            if ($merchant instanceof MerchantAccount) {
                $merchant_id_already_exists = true;
            } else {
                $merchant_id_already_exists = false;
            }
        } while ($merchant_id_already_exists);

        return $reference;
    }

    public static function numericString($type = 'int')
    {
        $reference = substr(explode(' ', microtime())[1], 0, 4).str_shuffle(explode('.', explode(' ', microtime())[0])[1]);

        if ($type == 'int') {
            return (int) $reference;
        }

        return $reference;
    }

    public static function uuidString()
    {
        return str_replace('-', '', Str::uuid()->toString());
    }
}
