<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 15/11/2020
 * Time: 4:11 PM.
 */

namespace App\Transformers;

use stdClass;

class ResponseStructure
{
    public static function success($data = null, $http_code = 200)
    {
        if ($data instanceof StdClass || is_array($data) || is_string($data)) {
            return response()->json($data, $http_code);
        }

        return response()->json(null, $http_code);
    }

    public static function error($message = 'Generic Error', $http_code = 900, $data = null)
    {
        $error = init();
        $response = init();

        $error->msg = $message;
        $error->code = $http_code;

        $error->data = is_null($data) ? init() : $data;

        $response->error = $error;

        return response()->json($response, $http_code);
    }
}
