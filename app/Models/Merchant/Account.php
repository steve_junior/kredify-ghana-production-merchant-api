<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 18/07/2020
 * Time: 9:43 PM.
 */

namespace App\Models\Merchant;

use App\Models\Order;
use App\Services\GeneratorService;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable
{
    protected $table = 'merchant_accounts';

    protected $fillable = [
                'name',
                'minimum_order_amount',
                'maximum_order_amount',
                'site',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'production_api_token', 'production_password', 'production_username',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($account) {
            $account->production_username = GeneratorService::generateMerchantUsername($account->name);
            $account->production_password = GeneratorService::generateMerchantPassword();
            $account->country = config(CONFIG_SETTINGS_COUNTRY);

            $account->merchant_id = GeneratorService::generateMerchantID();
            $account->production_api_token = GeneratorService::generateApiToken($account->production_username, $account->production_password);
        });
    }

    public function setMinimumOrderAmountAttribute($value)
    {
        $this->attributes['minimum_order_amount'] = jsonEncode($value);
    }

    public function getMinimumOrderAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setMaximumOrderAmountAttribute($value)
    {
        $this->attributes['maximum_order_amount'] = jsonEncode($value);
    }

    public function getMaximumOrderAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function placed_orders()
    {
        return $this->hasMany(Order::class);
    }
}
