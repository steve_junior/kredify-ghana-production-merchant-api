<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 19/07/2020
 * Time: 7:49 PM.
 */

namespace App\Models;

use App\Services\GeneratorService;
use Illuminate\Database\Eloquent\Model;
use App\Models\Merchant\Account as MerchantAccount;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'consumer',
        'order_recipient',
        'order_items',
        'delivery_cost',
        'discounts',
        'total_amount',
        'confirmation_url',
        'cancellation_url',
        'order_number',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($order) {
            $order->merchant_account_id = auth()->id();
            $order->system_generated_order_number = GeneratorService::generateOrderNumber();
        });
    }

    public function setConsumerAttribute($value)
    {
        $value['phoneNumber'] = getInternationalPhone($value['phoneNumber']);
        $this->attributes['consumer'] = jsonEncode($value);
    }

    public function getConsumerAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setTotalAmountAttribute($value)
    {
        $this->attributes['total_amount'] = jsonEncode($value);
    }

    public function getTotalAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setOrderItemsAttribute($value)
    {
        $this->attributes['order_items'] = jsonEncode($value);
    }

    public function getOrderItemsAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setDeliveryCostAttribute($value)
    {
        $this->attributes['delivery_cost'] = jsonEncode($value);
    }

    public function getDeliveryCostAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setOrderRecipientAttribute($value)
    {
        $value['phoneNumber'] = getInternationalPhone($value['phoneNumber']);
        $this->attributes['order_recipient'] = jsonEncode($value);
    }

    public function getOrderRecipientAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setDiscountsAttribute($value)
    {
        $this->attributes['discounts'] = jsonEncode($value);
    }

    public function getDiscountsAttribute($value)
    {
        return jsonDecode($value);
    }

    public function merchant()
    {
        return $this->belongsTo(MerchantAccount::class, 'merchant_account_id');
    }

    public function updateSimpleModel(array $attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
            $this->{$key} = $attribute;
        }

        return $this->save();
    }
}
