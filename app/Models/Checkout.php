<?php

namespace App\Models;

use App\Models\Merchant\PaymentEvent;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable = [
      'token', 'order_id',
    ];

    protected $dates = [
        'expired_at',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($checkout) {
            $checkout->token_url = config(CONFIG_SETTINGS_CHECKOUTURL).$checkout->token;
            $checkout->merchant_account_id = auth()->id();
            $checkout->expired_at = now()->addMonth(); //TODO ~ should probably be change to a few days
            $checkout->status = CLIENT_DORMANT;
        });
    }

    public function getStatusAttribute($value)
    {
        return customerCheckoutStatus($value);
    }

    public function hasExpired()
    {
        return $this->expired_at->isPast();
    }

    public function isApprovedByConsumer()
    {
        return $this->status == customerCheckoutStatus(CLIENT_APPROVED);
    }

    public function isDeclinedByConsumer()
    {
        return $this->status == customerCheckoutStatus(CLIENT_DECLINED);
    }

    public function isPendingByConsumer()
    {
        return $this->status == customerCheckoutStatus(CLIENT_DORMANT);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function merchantPaymentEvent()
    {
        return $this->hasOne(PaymentEvent::class);
    }
}
