<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_payment_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('total_amount')->nullable();
            $table->integer('status')->nullable();

            $table->unsignedBigInteger('event_id')->nullable();

            $table->unsignedBigInteger('checkout_id')->index();
            $table->unsignedBigInteger('merchant_account_id')->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_payment_events');
    }
}
