<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();

            $table->string('production_username')->index();
            $table->string('production_password')->index();
            $table->string('production_api_token')->index();

            // merchantID
            $table->unsignedBigInteger('merchant_id')->index()->unique();
            $table->boolean('is_certified_for_production')->default(false);
            $table->timestamp('certified_at')->nullable();
            $table->json('minimum_order_amount')->nullable();
            $table->json('maximum_order_amount')->nullable();

            $table->string('country')->nullable();
            $table->string('site')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['merchant_accounts'];

        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
    }
}
