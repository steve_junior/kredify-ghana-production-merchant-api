<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('order_number')->nullable();
            $table->string('confirmation_url')->nullable();
            $table->string('cancellation_url')->nullable();

            $table->json('consumer')->nullable();
            $table->json('order_recipient')->nullable();
            $table->json('order_items')->nullable();
            $table->json('delivery_cost')->nullable();
            $table->json('discounts')->nullable();
            $table->json('total_amount')->nullable();
            $table->unsignedBigInteger('system_generated_order_number')->index()->unique();

            $table->unsignedBigInteger('merchant_account_id')->index();
            $table->timestamps();
        });

        Schema::create('checkouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token')->unique()->index();
            $table->string('token_url')->unique()->index();
            $table->timestamp('expired_at')->nullable();
            $table->integer('status');

            $table->unsignedBigInteger('consumer_id')->index()->nullable();
            $table->unsignedBigInteger('merchant_account_id')->index();
            $table->unsignedBigInteger('order_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
        Schema::dropIfExists('orders');
    }
}
