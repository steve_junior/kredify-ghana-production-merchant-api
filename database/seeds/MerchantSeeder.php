<?php

use Illuminate\Database\Seeder;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $minimum_amount = init();
        $minimum_amount->value = 100;
        $minimum_amount->currency = config(CONFIG_SETTINGS_CURRENCY);

        $maximum_amount = init();
        $maximum_amount->value = 1000;
        $maximum_amount->currency = config(CONFIG_SETTINGS_CURRENCY);

        \App\Models\Merchant\Account::create([
            'name'                 => 'My Ecommerce Ghana Limited',
            'minimum_order_amount' => $minimum_amount,
            'maximum_order_amount' => $maximum_amount,
            'site'                 => 'https://myecommerce.com.gh',
        ]);
    }
}
